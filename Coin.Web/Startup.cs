﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Coin.Web.Startup))]
namespace Coin.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
