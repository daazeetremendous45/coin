/// <autosync enabled="true" />
/// <reference path="../assets/js/contact.js" />
/// <reference path="../assets/js/scripts.js" />
/// <reference path="../assets/js/view/demo.camera_slider.js" />
/// <reference path="../assets/js/view/demo.elastic_slider.js" />
/// <reference path="../assets/js/view/demo.graphs.flot.js" />
/// <reference path="../assets/js/view/demo.layerslider_slider.js" />
/// <reference path="../assets/js/view/demo.music.js" />
/// <reference path="../assets/js/view/demo.nivo_slider.js" />
/// <reference path="../assets/js/view/demo.portfolio_ajax.js" />
/// <reference path="../assets/js/view/demo.revolution_slider.js" />
/// <reference path="../assets/js/view/demo.shop.js" />
/// <reference path="../assets/js/view/demo.swiper_slider.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/js/bootstrap-datepicker.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.ar.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.az.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.bg.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.bs.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.ca.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.cs.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.cy.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.da.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.de.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.el.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.en-GB.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.es.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.et.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.eu.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.fa.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.fi.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.fo.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.fr.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.fr-CH.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.gl.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.he.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.hr.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.hu.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.hy.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.id.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.is.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.it.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.it-CH.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.ja.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.ka.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.kh.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.kk.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.kr.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.lt.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.lv.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.me.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.mk.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.ms.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.nb.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.nl.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.nl-BE.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.no.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.pl.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.pt.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.pt-BR.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.ro.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.rs.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.rs-latin.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.ru.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.sk.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.sl.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.sq.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.sr.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.sr-latin.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.sv.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.sw.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.th.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.tr.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.uk.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.vi.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.zh-CN.min.js" />
/// <reference path="../assets/plugins/bootstrap.datepicker/locales/bootstrap-datepicker.zh-TW.min.js" />
/// <reference path="../assets/plugins/bootstrap.daterangepicker/daterangepicker.js" />
/// <reference path="../assets/plugins/bootstrap.daterangepicker/moment.js" />
/// <reference path="../assets/plugins/bootstrap/js/bootstrap.js" />
/// <reference path="../assets/plugins/bootstrap/js/npm.js" />
/// <reference path="../assets/plugins/custom.fle_upload.js" />
/// <reference path="../assets/plugins/datatables/dataTables.bootstrap.js" />
/// <reference path="../assets/plugins/datatables/extensions/AutoFill/js/dataTables.autoFill.js" />
/// <reference path="../assets/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.js" />
/// <reference path="../assets/plugins/datatables/extensions/ColVis/js/dataTables.colVis.js" />
/// <reference path="../assets/plugins/datatables/extensions/FixedColumns/js/dataTables.fixedColumns.js" />
/// <reference path="../assets/plugins/datatables/extensions/FixedHeader/js/dataTables.fixedHeader.js" />
/// <reference path="../assets/plugins/datatables/extensions/KeyTable/js/dataTables.keyTable.js" />
/// <reference path="../assets/plugins/datatables/extensions/Responsive/js/dataTables.responsive.js" />
/// <reference path="../assets/plugins/datatables/extensions/Scroller/js/dataTables.scroller.js" />
/// <reference path="../assets/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.js" />
/// <reference path="../assets/plugins/datatables/js/dataTables.bootstrap.js" />
/// <reference path="../assets/plugins/datatables/js/dataTables.colReorder.min.js" />
/// <reference path="../assets/plugins/datatables/js/dataTables.foundation.js" />
/// <reference path="../assets/plugins/datatables/js/dataTables.jqueryui.js" />
/// <reference path="../assets/plugins/datatables/js/dataTables.scroller.min.js" />
/// <reference path="../assets/plugins/datatables/js/dataTables.tableTools.min.js" />
/// <reference path="../assets/plugins/datatables/js/jquery.dataTables.js" />
/// <reference path="../assets/plugins/editor.markdown/js/bootstrap-markdown.js" />
/// <reference path="../assets/plugins/editor.markdown/locale/bootstrap-markdown.ar.js" />
/// <reference path="../assets/plugins/editor.markdown/locale/bootstrap-markdown.de.js" />
/// <reference path="../assets/plugins/editor.markdown/locale/bootstrap-markdown.es.js" />
/// <reference path="../assets/plugins/editor.markdown/locale/bootstrap-markdown.fr.js" />
/// <reference path="../assets/plugins/editor.markdown/locale/bootstrap-markdown.ja.js" />
/// <reference path="../assets/plugins/editor.markdown/locale/bootstrap-markdown.kr.js" />
/// <reference path="../assets/plugins/editor.markdown/locale/bootstrap-markdown.nb.js" />
/// <reference path="../assets/plugins/editor.markdown/locale/bootstrap-markdown.nl.js" />
/// <reference path="../assets/plugins/editor.markdown/locale/bootstrap-markdown.pl.js" />
/// <reference path="../assets/plugins/editor.markdown/locale/bootstrap-markdown.ru.js" />
/// <reference path="../assets/plugins/editor.markdown/locale/bootstrap-markdown.sl.js" />
/// <reference path="../assets/plugins/editor.markdown/locale/bootstrap-markdown.sv.js" />
/// <reference path="../assets/plugins/editor.markdown/locale/bootstrap-markdown.tr.js" />
/// <reference path="../assets/plugins/editor.markdown/locale/bootstrap-markdown.ua.js" />
/// <reference path="../assets/plugins/editor.markdown/locale/bootstrap-markdown.zh.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-ar-AR.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-ca-ES.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-cs-CZ.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-da-DK.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-de-DE.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-es-ES.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-es-EU.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-fa-IR.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-fi-FI.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-fr-FR.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-he-IL.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-hu-HU.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-id-ID.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-it-IT.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-ja-JP.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-ko-KR.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-nb-NO.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-nl-NL.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-pl-PL.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-pt-BR.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-ro-RO.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-ru-RU.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-sk-SK.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-sl-SI.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-sr-RS.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-sr-RS-Latin.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-sv-SE.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-th-TH.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-tr-TR.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-uk-UA.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-vi-VN.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-zh-CN.js" />
/// <reference path="../assets/plugins/editor.summernote/lang/summernote-zh-TW.js" />
/// <reference path="../assets/plugins/editor.summernote/summernote.js" />
/// <reference path="../assets/plugins/effects/canvas.particles.js" />
/// <reference path="../assets/plugins/footable/dist/footable.all.min.js" />
/// <reference path="../assets/plugins/footable/dist/footable.bookmarkable.min.js" />
/// <reference path="../assets/plugins/footable/dist/footable.filter.min.js" />
/// <reference path="../assets/plugins/footable/dist/footable.min.js" />
/// <reference path="../assets/plugins/footable/dist/footable.paginate.min.js" />
/// <reference path="../assets/plugins/footable/dist/footable.sort.min.js" />
/// <reference path="../assets/plugins/footable/dist/footable.striping.min.js" />
/// <reference path="../assets/plugins/footable/js/footable.bookmarkable.js" />
/// <reference path="../assets/plugins/footable/js/footable.filter.js" />
/// <reference path="../assets/plugins/footable/js/footable.grid.js" />
/// <reference path="../assets/plugins/footable/js/footable.js" />
/// <reference path="../assets/plugins/footable/js/footable.memory.js" />
/// <reference path="../assets/plugins/footable/js/footable.paginate.js" />
/// <reference path="../assets/plugins/footable/js/footable.plugin.template.js" />
/// <reference path="../assets/plugins/footable/js/footable.sort.js" />
/// <reference path="../assets/plugins/footable/js/footable.striping.js" />
/// <reference path="../assets/plugins/form.masked/jquery.maskedinput.js" />
/// <reference path="../assets/plugins/form.slidebar/jquery-ui-slider-pips.min.js" />
/// <reference path="../assets/plugins/form.stepper/jquery.stepper.min.js" />
/// <reference path="../assets/plugins/form.validate/jquery.form.min.js" />
/// <reference path="../assets/plugins/form.validate/jquery.validation.min.js" />
/// <reference path="../assets/plugins/gmaps.js" />
/// <reference path="../assets/plugins/image.zoom/jquery.zoom.js" />
/// <reference path="../assets/plugins/infinite-scroll/jquery.infinitescroll.js" />
/// <reference path="../assets/plugins/isotope/isotope.pkgd.js" />
/// <reference path="../assets/plugins/jqgrid/js/grid.base.js" />
/// <reference path="../assets/plugins/jqgrid/js/grid.celledit.js" />
/// <reference path="../assets/plugins/jqgrid/js/grid.common.js" />
/// <reference path="../assets/plugins/jqgrid/js/grid.custom.js" />
/// <reference path="../assets/plugins/jqgrid/js/grid.filter.js" />
/// <reference path="../assets/plugins/jqgrid/js/grid.formedit.js" />
/// <reference path="../assets/plugins/jqgrid/js/grid.grouping.js" />
/// <reference path="../assets/plugins/jqgrid/js/grid.import.js" />
/// <reference path="../assets/plugins/jqgrid/js/grid.inlinedit.js" />
/// <reference path="../assets/plugins/jqgrid/js/grid.jqueryui.js" />
/// <reference path="../assets/plugins/jqgrid/js/grid.pivot.js" />
/// <reference path="../assets/plugins/jqgrid/js/grid.subgrid.js" />
/// <reference path="../assets/plugins/jqgrid/js/grid.tbltogrid.js" />
/// <reference path="../assets/plugins/jqgrid/js/grid.treegrid.js" />
/// <reference path="../assets/plugins/jqgrid/js/jqDnR.js" />
/// <reference path="../assets/plugins/jqgrid/js/jqModal.js" />
/// <reference path="../assets/plugins/jqgrid/js/jquery.fmatter.js" />
/// <reference path="../assets/plugins/jqgrid/js/jquery.jqGrid.js" />
/// <reference path="../assets/plugins/jqgrid/js/JsonXml.js" />
/// <reference path="../assets/plugins/jqgrid/js/minified/jquery.jqGrid.min.js" />
/// <reference path="../assets/plugins/jqgrid/plugins/grid.addons.js" />
/// <reference path="../assets/plugins/jqgrid/plugins/grid.postext.js" />
/// <reference path="../assets/plugins/jqgrid/plugins/grid.setcolumns.js" />
/// <reference path="../assets/plugins/jqgrid/plugins/jquery.contextmenu.js" />
/// <reference path="../assets/plugins/jqgrid/plugins/jquery.searchFilter.js" />
/// <reference path="../assets/plugins/jqgrid/plugins/jquery.tablednd.js" />
/// <reference path="../assets/plugins/jqgrid/plugins/ui.multiselect.js" />
/// <reference path="../assets/plugins/jquery.backstretch.min.js" />
/// <reference path="../assets/plugins/jquery.mb.YTPlayer.min.js" />
/// <reference path="../assets/plugins/jquery.nav.min.js" />
/// <reference path="../assets/plugins/jquery/jquery.ui.touch-punch.min.js" />
/// <reference path="../assets/plugins/jquery/jquery-2.1.4.min.js" />
/// <reference path="../assets/plugins/jquery/jquery-ui.min.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/jquery.vmap.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/jquery.vmap.packed.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/maps/continents/jquery.vmap.africa.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/maps/continents/jquery.vmap.asia.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/maps/continents/jquery.vmap.australia.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/maps/continents/jquery.vmap.europe.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/maps/continents/jquery.vmap.north-america.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/maps/continents/jquery.vmap.south-america.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.algeria.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.brazil.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.france.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" />
/// <reference path="../assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" />
/// <reference path="../assets/plugins/lazyload/jquery.lazyload.js" />
/// <reference path="../assets/plugins/magnific-popup/jquery.magnific-popup.js" />
/// <reference path="../assets/plugins/masonry.pkgd.min.js" />
/// <reference path="../assets/plugins/mediaelement/mediaelement.js" />
/// <reference path="../assets/plugins/mediaelement/mediaelement-and-player.js" />
/// <reference path="../assets/plugins/mediaelement/mediaelementplayer.js" />
/// <reference path="../assets/plugins/mixitup/jquery.mixitup.min.js" />
/// <reference path="../assets/plugins/owl-carousel/owl.carousel.js" />
/// <reference path="../assets/plugins/pajinate/jquery.pajinate.bootstrap.min.js" />
/// <reference path="../assets/plugins/pajinate/jquery.pajinate.js" />
/// <reference path="../assets/plugins/select2/js/select2.full.js" />
/// <reference path="../assets/plugins/select2/js/select2.js" />
/// <reference path="../assets/plugins/slimscroll/jquery.slimscroll.js" />
/// <reference path="../assets/plugins/smoothscroll.js" />
/// <reference path="../assets/plugins/spectrum/spectrum.js" />
/// <reference path="../assets/plugins/styleswitcher/styleswitcher.js" />
/// <reference path="../assets/plugins/text-rotator/jquery.simple-text-rotator.js" />
/// <reference path="../assets/plugins/timepicki/timepicki.js" />
/// <reference path="../assets/plugins/toastr/toastr.js" />
/// <reference path="../assets/plugins/typeahead.bundle.js" />
/// <reference path="../assets/plugins/widget.dribbble/jribbble.js" />
/// <reference path="../assets/plugins/widget.jflickr/colorbox/jquery.colorbox-min.js" />
/// <reference path="../assets/plugins/widget.jflickr/cycle/jquery.cycle.all.min.js" />
/// <reference path="../assets/plugins/widget.jflickr/jflickrfeed.js" />
/// <reference path="../assets/plugins/widget.jflickr/setup.js" />
/// <reference path="../assets/plugins/widget.twittie/twittie.min.js" />
/// <reference path="bootstrap.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery.validate.unobtrusive.js" />
/// <reference path="jquery-1.10.2.js" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="respond.js" />
